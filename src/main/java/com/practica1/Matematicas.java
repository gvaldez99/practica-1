package com.practica1;


public class Matematicas {
    public static double generarNumeroPi(long pasos){
        int radioCirculo = 1;
        long puntosDentro = 0;
        for(long i=0;i<pasos;i++){
            double X = Math.random() * (1 - -1)+ -1;
            double Y = Math.random() * (1 - -1)+ -1;
            double distOrigen = Math.sqrt(X*X+Y*Y);
            if(distOrigen <= 1) puntosDentro+=1;
        }
        
        return ((double)puntosDentro/pasos*4);
    }
}
