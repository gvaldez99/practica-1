Proyecto hecho con Apache Maven, compilar utilizando como main class la clase Principal

Este codigo calcula el numero pi mediante el metodo Montecarlo, este algoritmo genera coordenadas aleatorias dentro de un cuadrado
y compara la cantidad de coordenadas dentro del cuadrado, y las que estan dentro de la zona circunscrita.
De esta forma, al realizar la ecuacion PuntosDentro/PuntosTotales * 4, consigues un numero muy cercano a pi.

Utiliza un metodo en la clase Matematicas el cual puedes utilizar importandola a tu proyecto, solo debes usar el metodo 
~~~
double pi = Matematicas.generarNumeroPi(int n) 

~~~
El parametro int son las iteraciones realizadas, a mas iteraciones mas precision tiene el numero regresado por el metodo.

Alumno: Gonzalo Valdez